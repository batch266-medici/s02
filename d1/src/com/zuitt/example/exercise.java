package com.zuitt.example;

import java.util.Scanner;

public class exercise {
    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        System.out.println("Enter a year: ");
        int isLeapYear = userInput.nextInt();
        if (isLeapYear % 4 == 0) {
            System.out.println(isLeapYear + " is a leap year.");
        } else {
            System.out.println(isLeapYear + " is not a leap year.");
        }
    }
}
